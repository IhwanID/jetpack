# Jetpack

## Implement Jetpack Library

* [Data Binding](https://github.com/IhwanID/jetpack/tree/data-binding)
* [Lifecycle Aware](https://github.com/IhwanID/jetpack/tree/lifecycle-aware)
* [Viewmodel](https://github.com/IhwanID/jetpack/tree/viewmodel)
* [Work Manager](https://github.com/IhwanID/jetpack/tree/workmanager)